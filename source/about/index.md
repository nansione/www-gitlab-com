---
layout: markdown_page
title: About Us
---

## GitLab B.V.

GitLab B.V is the company behind the [GitLab open-source project](https://gitlab.com/gitlab-org/gitlab-ce/)
which is hosted on [GitLab.com](https://gitlab.com) (our free hosted service).
GitLab is a Rails application providing Git repository management with fine grained
access controls, code reviews, issue tracking, activity feeds, wikis and continuous integration.
GitLab B.V has 4 offerings, 3 of which are free:

1. [GitLab.com](https://about.gitlab.com/gitlab-com/) - free SaaS for public and private repositories, support can be purchased
1. [GitLab Community Edition (CE)](https://about.gitlab.com/features/#community) - free, self hosted application, support from [Community](https://about.gitlab.com/getting-help/)
1. [GitLab Enterprise Edition (EE)](https://about.gitlab.com/pricing/) - paid, self hosted application, comes with additional features and support
1. [GitLab Continuous Integration (CI)](https://about.gitlab.com/gitlab-ci/) - free, self hosted application that integrates with GitLab CE/EE. Also availble as SaaS at [ci.gitlab.com](https://ci.gitlab.com)

GitLab B.V also offers:

1. [Git and GitLab Training](https://about.gitlab.com/training/)
1. [Consulting](https://about.gitlab.com/consultancy/)
1. [Custom Development work](https://about.gitlab.com/development/)

GitLab B.V has two reseller partners in the US:

* [ReleaseTEAM](http://www.releaseteam.com)
* [WANdisco](http://www.wandisco.com/press-releases/wandisco-git-multisite-adds-gitlab-support)

GitLab B.V. supports organizations and individuals using GitLab.
To do this we offer services around GitLab such as subscriptions, consulting, development and training.

Above all, GitLab is a community project, over 800 people worldwide have [contributed to GitLab]!
GitLab B.V. is an active participant in this community, trying to serve its needs and lead by example.


## A brief history of GitLab

### _2011: Start of GitLab_

In 2011 Dmitriy was unsatisfied with the options for git repository management.
So together with Valery, he started to build GitLab as a solution for this.

[This commit] was the very start of GitLab.

### _2012: GitLab.com_

Sytse began a SaaS of GitLab, which until then was only an open source project
that could be ran on your own servers. GitLab.com offered free and paid hosting
of git projects to anyone.

In November 2012, Dmitriy also made the [first version of GitLab CI].

### _2013: "I want to work on GitLab full time"_

In 2013, Dmitriy tweeted that he wanted to work on GitLab full time.
Sytse and Dmitriy teamed up and started bootstrapping GitLab as a company.

In the same year, we introduced [GitLab Enterprise Edition].

### _2014: GitLab Incorporated_

In 2014 GitLab was officially incorporated in the Netherlands as GitLab B.V..

GitLab released a new version every month in 2014, just as every year before it.
The first release of the year at January 22nd: GitLab 6.5. At the end of 2014, December 2014, GitLab
7.6 was released.

### _2015: Y Combinator_

In the very start of 2015, almost the entire GitLab B.V. team flew over to Silicon
Valley to [participate in Y Combinator].

At this point, over 800 people worldwide have [contributed to GitLab] and more
than 100,000 organizations are using GitLab.

## Vision
Our vision is that every digital product that is created by more than one person
should be managed in distributed version control.
This allows people to cooperate effectively and to achieve better results, faster.

## Mission and logo
We are part of a community that works together to create the best open source collaboration tools.
Our <a href="https://en.wikipedia.org/wiki/Raccoon_dog">racoon dog</a> logo symbolizes this with a smart animal that works in a group to achieve a common goal.
We would like to thank <a href="https://twitter.com/gravityonmars">Ricardo Rauch</a> for contributing the logo.

## Values
We care about giving back to the rest of the GitLab community, that is why we give most code back to the <a href="https://gitlab.com/gitlab-org/gitlab-ce/">Community Edition</a>.
We try to be open, that is why this website is maintained in <a href="https://gitlab.com/gitlab-com/www-gitlab-com/">a public repository</a>.
We realize that GitLab is more than this organization, this is shown by having people from outside this company in the <a href="https://www.gitlab.com/core-team/">core team</a>.
We think good code can be written by anyone, we encourage diversity by contributing to <a href="http://railsgirls.nl/">RailsGirls NL</a>.
We try to correct the mistakes we make, please give us a chance by <a href="mailto:contact@gitlab.com">emailing us</a>.

## Handbook
If you're interested, most of our internal procedures can be found in <a href="/handbook">publicly viewable handbooks</a>.

## Donations
Some people contact us because they would like to donate to GitLab.
If you have time to give please help spread the word about GitLab by mentioning us and/or <a href="https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md">contribute by reviewing issues and merge requests</a>.
If you would like to give money please <a href="http://railsgirlssummerofcode.org/campaign/">donate to Rails Girls Summer of Code</a> in our name.

## Location
GitLab is a community project with people participating from all over the world.
GitLab B.V. has people on three continents to serve our customers better.
Most of our customers are in the United States and we have a fully owned subsidiary GitLab Inc. there.
Most of our team is distrubuted, we're proud to be [working remote](https://about.gitlab.com/2015/04/08/the-remote-manifesto/) using GitLab issues to coordinate.

[This commit]: https://gitlab.com/gitlab-org/gitlab-ce/commit/0f43e98ef8c2da8908b1107f75b67cda2572c2c4
[first version of GitLab CI]: https://gitlab.com/gitlab-org/gitlab-ci/commit/52cd500ee64a4a82b9ff6752ee85028cd419fcbe
[GitLab Enterprise Edition]: https://about.gitlab.com/2013/08/22/introducing-gitlab-6-0-enterprise-edition/
[participate in Y Combinator]: https://about.gitlab.com/2015/03/04/gitlab-is-part-of-the-y-combinator-family/
[contributed to GitLab]: http://contributors.gitlab.com/
